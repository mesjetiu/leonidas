Leonidas
========

Este programa tiene como fin practicar y aprender diferentes disciplinas de programación. Cualquiera puede copiarlo y modificarlo. Estaré encantado de ver que otros desarrolladores partan de mi pequeño proyecto para continuarlo y mejorarlo. Por esta razón creo que es clave la claridad y la legibilidad desde el principio.

Usando c++ como lenguaje, se dará prioridad al código claro y limpio frente a la pura eficiencia. El objetivo será pues el de optimizar el código para que sin perder legibilidad y capacidad de modificación pueda ser rápido y eficiente.

Las funciones y clases estarán documentadas con Doxygen.

El repositorio Git seguirá las convenciones más usuales de branching. Así, contendrá la rama 'master' para las versiones funcionales con número de versión, mientras que se desarrollará sobre la rama 'develop'. Así mismo se tendrán ramas temporales para 'features', 'hotfixes' y 'releases'. De este modo se conservará la historia del programa de forma ordenada, clara y convencional. Información sobre este Git Workflow se puede encontrar en http://nvie.com/posts/a-successful-git-branching-model/

El motor jugará al ajedrez con todas las normas oficiales. Se comunicará con interfaces gráficas con la entrada y salida estandar (consola) usando el protocolo xboard y UCI.

Cualquier sugerencia y colaboración será bienvenida.
